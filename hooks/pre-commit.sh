#!/usr/bin/env bash

BASE_DIR=./lambdas/

AFFECTED=$(git diff HEAD --dirstat=files,0 ${BASE_DIR} | awk 'match($0, /lambdas\/[^\/]+?\/$/){print substr($0, RSTART, RLENGTH)}')

for DIR in ${AFFECTED}
do
    VERSION_FILE=${DIR}.version
    git diff HEAD ${DIR} | git hash-object --stdin > ${VERSION_FILE}
    git add ${VERSION_FILE}
done
