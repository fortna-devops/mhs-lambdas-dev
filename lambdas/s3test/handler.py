import json
import logging

import botocore
import boto3
import s3fs


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()


SESSION = boto3.Session()
CONFIG_BUCKET_NAME = 'mhs-analytics-config'
FILE_NAME = 'ses_heartbeat'
S3_PATH = f'{CONFIG_BUCKET_NAME}/{FILE_NAME}'


def read_s3fs(max_attempts):
    s3_config_kwargs = {'retries': {'max_attempts': max_attempts}}
    fs = s3fs.S3FileSystem(session=SESSION, config_kwargs=s3_config_kwargs)
    with fs.open(S3_PATH) as cf:
        contents = cf.read().decode()
    return contents


def read_resource(max_attempts):
    config = botocore.client.Config(
        connect_timeout=5,  # hardcoded in s3f3 lib
        read_timeout=15,  # hardcoded in s3f3 lib
        retries={'max_attempts': max_attempts}
    )
    s3 = SESSION.resource('s3', config=config)
    response = s3.get_object(
        Bucket=CONFIG_BUCKET_NAME,
        Key=FILE_NAME
    )
    return response['Body'].read()


HANDLER_MAP = {
    'read_s3fs': read_s3fs,
    'read_resource': read_resource
}


def handle(function_name, max_attempts):
    func = HANDLER_MAP[function_name]
    result = func(max_attempts)
    print(result)


def lambda_handler(event, _context):
    function_name = event['function_name']
    invoke_type = event.get('invoke_type', 'loop')
    iterations = event.get('iterations', 1)
    max_attempts = event.get('max_attempts', 0)

    if invoke_type == 'loop':
        for i in range(iterations):
            handle(function_name, max_attempts)

    elif invoke_type == 'async':
        session = boto3.Session()
        lambda_client = session.client('lambda')
        for i in range(iterations):
            lambda_client.invoke(
                    FunctionName='s3test',
                    InvocationType='Event',
                    Payload=json.dumps({'function_name': function_name, 'max_attempts': max_attempts}),
                    Qualifier='master'
                )

    else:
        print('Unknown invoke type')


if __name__ == '__main__':
    lambda_handler({
        'function_name': 'read_s3fs'
    }, {})
