import unittest

from lambda_function import lambda_handler


class DemoTest(unittest.TestCase):

    def test_requirements(self):
        try:
            import boto3
        except ImportError:
            self.fail('Requirements are not installed')

    @unittest.skip('demonstrating skipping')
    def test_skipped(self):
        self.fail('shouldn\'t happen')

    # def test_fail(self):
    #     self.fail('should fail')
