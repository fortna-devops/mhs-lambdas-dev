Tris repository is a test platform for MHS-CI.

You don't need to use it unless you making changes in CI.

Basically this is a copy of mhs-lambdas repository except:

* it using *develop* image for pipelines 
* have only one lambda for testing purposes
